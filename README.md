# LeagueOfQuiz <img alt="Version 1.0.0" src="https://img.shields.io/badge/Version-1.0.0-green.svg" />
A Quiz of images and you need to guess the right champion(from League of legends game) behind this hidden/shown images :
# ScreenShots 
<ul>
 <li>Singin Screen : <br>
<img  alt="Signin" src="https://github.com/HoussemTN/LeagueOfQuiz/blob/master/screenshots/siginin.PNG?raw=true" heigth="350px" width="300px"/></li> 
<li>Menu Screen : <br>
<img  alt="Menu" src="https://github.com/HoussemTN/LeagueOfQuiz/blob/master/screenshots/Menu.PNG?raw=true" heigth="350px" width="300px"/> </li>
 <li>Game Screen : <br>
<img  alt="Game" src="https://github.com/HoussemTN/LeagueOfQuiz/blob/master/screenshots/game.PNG?raw=true" heigth="350px" width="300px"/> </li>
 <li>Shop Screen : <br>
<img  alt="Shop" src="https://github.com/HoussemTN/LeagueOfQuiz/blob/master/screenshots/shop.PNG?raw=true" heigth="350px" width="300px"/> </li></ul>
